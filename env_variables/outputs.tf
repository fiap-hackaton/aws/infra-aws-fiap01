
//vpc
output "VpcCidrBlock" {
  value = var.VpcCidrBlock
}
output "PublicSubnetCidrBlock1" {
  value = var.PublicSubnetCidrBlock1
}
output "PublicSubnetCidrBlock2" {
  value = var.PublicSubnetCidrBlock2
}
output "PublicSubnetCidrBlock3" {
  value = var.PublicSubnetCidrBlock3
}
output "PrivateSubnetCidrBlock1" {
  value = var.PrivateSubnetCidrBlock1
}
output "PrivateSubnetCidrBlock2" {
  value = var.PrivateSubnetCidrBlock2
}
output "PrivateSubnetCidrBlock3" {
  value = var.PrivateSubnetCidrBlock3
}
output "Region" {
  value = var.Region
}
output "AvailabilityZone1" {
  value = var.AvailabilityZone1
}
output "AvailabilityZone2" {
  value = var.AvailabilityZone2
}
output "AvailabilityZone3" {
  value = var.AvailabilityZone3
}

//ec2
output "KeyPairName" {
  value = var.KeyPairName
}
output "Rhel8AMI" {
  value = var.Rhel8AMI
}
output "countvalue" {
  value = var.countvalue
}