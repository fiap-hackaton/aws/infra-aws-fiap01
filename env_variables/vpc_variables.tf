variable "VpcCidrBlock" {
  default = "10.0.0.0/16"
}
variable "PublicSubnetCidrBlock1" {
  default = "10.0.0.0/24"
}
variable "PublicSubnetCidrBlock2" {
  default = "10.0.1.0/24"
}
variable "PublicSubnetCidrBlock3" {
  default = "10.0.10.0/24"
}
variable "PrivateSubnetCidrBlock1" {
  default = "10.0.11.0/24"
}
variable "PrivateSubnetCidrBlock2" {
  default = "10.0.20.0/24"
}
variable "PrivateSubnetCidrBlock3" {
  default = "10.0.21.0/24"
}
variable "Region" {
  default = "us-east-1"
}
variable "AvailabilityZone1" {
  default = "us-east-1a"
}
variable "AvailabilityZone2" {
  default = "us-east-1b"
}
variable "AvailabilityZone3" {
  default = "us-east-1c"
}