terraform {
  required_version = ">=0.12"
  backend "s3" {}
}

provider "aws" {
}

module env_variables {
  source = "./env_variables"
}


variable "environment_name_dev" {
  default = "fiaphackaton-dev"
}

module "network" {
  source                    = "./modules/network"

  AvailabilityZone1         = module.env_variables.AvailabilityZone1
  AvailabilityZone2         = module.env_variables.AvailabilityZone2
  AvailabilityZone3         = module.env_variables.AvailabilityZone3
  PrivateSubnetCidrBlock1   = module.env_variables.PrivateSubnetCidrBlock1
  PrivateSubnetCidrBlock2   = module.env_variables.PrivateSubnetCidrBlock2
  PrivateSubnetCidrBlock3   = module.env_variables.PrivateSubnetCidrBlock3
  PublicSubnetCidrBlock1    = module.env_variables.PublicSubnetCidrBlock1
  PublicSubnetCidrBlock2    = module.env_variables.PublicSubnetCidrBlock2
  PublicSubnetCidrBlock3    = module.env_variables.PublicSubnetCidrBlock3
  VpcCidrBlock              = module.env_variables.VpcCidrBlock
  environment_name          = var.environment_name_dev

}

module "ec2" {
  source                    = "./modules/ec2"

  FrontendSecurityGroup     = module.network.FrontendSecurityGroup
  KeyPairName               = module.env_variables.KeyPairName
  Rhel8AMI                 = module.env_variables.Rhel8AMI
  environment_name          = var.environment_name_dev
  private-subnet-1          = module.network.private-subnet-1
  public-subnet-1           = module.network.public-subnet-1
  public-subnet-2           = module.network.public-subnet-2
  public-subnet-3           = module.network.public-subnet-3
  vpc                       = module.network.vpc
  countvalue            = module.env_variables.countvalue

}

variable "environment_name_prod" {
  default = "fiaphackaton-prod"
}

module "network_prod" {
  source                    = "./modules/network"

  AvailabilityZone1         = module.env_variables.AvailabilityZone1
  AvailabilityZone2         = module.env_variables.AvailabilityZone2
  AvailabilityZone3         = module.env_variables.AvailabilityZone3
  PrivateSubnetCidrBlock1   = module.env_variables.PrivateSubnetCidrBlock1
  PrivateSubnetCidrBlock2   = module.env_variables.PrivateSubnetCidrBlock2
  PrivateSubnetCidrBlock3   = module.env_variables.PrivateSubnetCidrBlock3
  PublicSubnetCidrBlock1    = module.env_variables.PublicSubnetCidrBlock1
  PublicSubnetCidrBlock2    = module.env_variables.PublicSubnetCidrBlock2
  PublicSubnetCidrBlock3    = module.env_variables.PublicSubnetCidrBlock3
  VpcCidrBlock              = module.env_variables.VpcCidrBlock
  environment_name          = var.environment_name_prod
}

module "ec2_prod" {
  source                    = "./modules/ec2"

  FrontendSecurityGroup     = module.network.FrontendSecurityGroup
  KeyPairName               = module.env_variables.KeyPairName
  Rhel8AMI                  = module.env_variables.Rhel8AMI
  environment_name          = var.environment_name_prod
  private-subnet-1          = module.network.private-subnet-1
  public-subnet-1           = module.network.public-subnet-1
  public-subnet-2           = module.network.public-subnet-2
  public-subnet-3           = module.network.public-subnet-3
  vpc                       = module.network.vpc
  countvalue            = module.env_variables.countvalue
}