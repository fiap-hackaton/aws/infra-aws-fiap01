
resource "aws_instance" "rhel8" {
  ami                         = var.Rhel8AMI
  ebs_optimized               = false
  instance_type               = "t2.micro"
  monitoring                  = false
  key_name                    = var.KeyPairName
  subnet_id                   = var.private-subnet-1
  security_groups             = [var.FrontendSecurityGroup]
  associate_public_ip_address = true
  source_dest_check           = true
  count = var.countvalue
  root_block_device {
    volume_type           = "gp2"
    volume_size           = 50
    delete_on_termination = true
  }

  ebs_block_device {
    device_name           = "/dev/xvdf"
    snapshot_id           = ""
    volume_type           = "gp2"
    volume_size           = 50
    delete_on_termination = true
  }

  tags = {
    Name = format("nginx-workspace-%03d-${var.environment_name}", count.index + 1)
  }
}

resource "aws_lb" "torhel" {
  name               = "${var.environment_name}-torhel"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.FrontendSecurityGroup]
  subnets            = [var.public-subnet-1, var.public-subnet-2, var.public-subnet-3]

  enable_deletion_protection = true

  depends_on = [aws_instance.rhel8]
}

resource "aws_lb_target_group" "torhel" {
  name     = "${var.environment_name}-toubunto-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc

  depends_on = [aws_lb.torhel]
}

resource "aws_lb_listener" "torhel" {
  load_balancer_arn = aws_lb.torhel.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.torhel.arn
  }

  depends_on = [aws_lb_target_group.torhel]
}



