output "private-subnet-1" {
  value = aws_subnet.private-subnet-1.id
}
output "public-subnet-1" {
  value = aws_subnet.public-subnet-1.id
}
output "public-subnet-2" {
  value = aws_subnet.public-subnet-2.id
}
output "public-subnet-3" {
  value = aws_subnet.public-subnet-3.id
}
output "FrontendSecurityGroup" {
  value = aws_security_group.FrontendSecurityGroup.id
}
output "vpc" {
  value = aws_vpc.vpc.id
}