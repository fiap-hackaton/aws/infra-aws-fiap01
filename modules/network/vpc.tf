resource "aws_vpc" "vpc" {
  cidr_block = var.VpcCidrBlock
  enable_dns_support = true
  enable_dns_hostnames = true
  instance_tenancy = "default"
  tags = {
    Name = "${var.environment_name}-vpc"
  }
}



resource "aws_subnet" "public-subnet-1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.PublicSubnetCidrBlock1
  availability_zone       = var.AvailabilityZone1
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.environment_name}-public-subnet-1"
  }
}

resource "aws_subnet" "public-subnet-2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.PublicSubnetCidrBlock2
  availability_zone       = var.AvailabilityZone2
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.environment_name}-public-subnet-2"
  }
}

resource "aws_subnet" "public-subnet-3" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.PublicSubnetCidrBlock3
  availability_zone       = var.AvailabilityZone3
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.environment_name}-public-subnet-3"
  }
}

resource "aws_subnet" "private-subnet-1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.PrivateSubnetCidrBlock1
  availability_zone       = var.AvailabilityZone1
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.environment_name}-private-subnet-1"
  }
}

resource "aws_subnet" "private-subnet-2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.PrivateSubnetCidrBlock2
  availability_zone       = var.AvailabilityZone2
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.environment_name}-private-subnet-2"
  }
}

resource "aws_subnet" "private-subnet-3" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.PrivateSubnetCidrBlock3
  availability_zone       = var.AvailabilityZone3
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.environment_name}-private-subnet-3"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "${var.environment_name}-igw"
  }
}

resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.NATGatewayEIP.id
  subnet_id = aws_subnet.public-subnet-1.id
  tags = {
    Name = "${var.environment_name}-gwNAT"
  }
  depends_on = [
    aws_internet_gateway.igw
  ]
}
resource "aws_eip" "NATGatewayEIP" {
  vpc = true
}
resource "aws_route_table" "public-rt" {
  vpc_id     = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "${var.environment_name}-public-rt"
  }
}
resource "aws_route_table" "private-rt" {
  vpc_id     = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat.id
  }

  tags = {
    Name = "${var.environment_name}-private-rt"
  }
}
resource "aws_route_table_association" "public1" {
  route_table_id = aws_route_table.public-rt.id
  subnet_id = aws_subnet.public-subnet-1.id
}
resource "aws_route_table_association" "public2" {
  route_table_id = aws_route_table.public-rt.id
  subnet_id = aws_subnet.public-subnet-2.id
}
resource "aws_route_table_association" "public3" {
  route_table_id = aws_route_table.public-rt.id
  subnet_id = aws_subnet.public-subnet-3.id
}
resource "aws_route_table_association" "private1" {
  route_table_id = aws_route_table.private-rt.id
  subnet_id = aws_subnet.private-subnet-1.id
}
resource "aws_route_table_association" "private2" {
  route_table_id = aws_route_table.private-rt.id
  subnet_id = aws_subnet.private-subnet-2.id
}
resource "aws_route_table_association" "private3" {
  route_table_id = aws_route_table.private-rt.id
  subnet_id = aws_subnet.private-subnet-3.id
}